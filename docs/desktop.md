[Home](./index.md)

# Desktop Application

Eternal-Twin provides a desktop application. It allows you to play Motion Twin's Flash games.

## Download

Latest version: 0.5.2 (2021-01-31)

- [Windows 64-bit installer](https://eternal-twin.net/assets/app/EternaltwinSetup-0.5.2-x64.exe)
- [Windows 32-bit installer](https://eternal-twin.net/assets/app/EternaltwinSetup-0.5.2-ia32.exe)
- [Linux - Pacman package](https://eternal-twin.net/assets/app/etwin-0.5.2.pacman)
- [Linux - Debian package](https://eternal-twin.net/assets/app/etwin_0.5.2_amd64.deb)
- [Linux - Archive](https://eternal-twin.net/assets/app/etwin-0.5.2.tar.gz)
- [Mac installer](https://eternal-twin.net/assets/app/Eternaltwin-0.5.2.dmg)

## Contribute

[Repository](https://gitlab.com/eternal-twin/etwin-app)

The application is still in a beta state: please help us to improve it.

In particular, we need to ensure the app only tries to open trusted websites
(from Eternal-Twin or Motion-Twin).

## Issues

Report issues [on Gitlab](https://gitlab.com/eternal-twin/etwin-app/-/issues).
