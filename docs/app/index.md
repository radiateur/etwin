[Home](../index.md) | [Applications](./index.md)

# Application Development

This section provides guides to help when developing Eternal-Twin applications and games.

- [Guidelines](./guidelines.md)
- [Application Configuration](./config.md)
- [Eternal-Twin integration](./etwin-integration.md)
- [Eternal-Twin for OAuth](./etwin-oauth.md)
- [Eternal-Twin API](./etwin-api.md)
