import { $Uint32, IntegerType } from "kryo/lib/integer.js";

export type RawTwinoidUserId = number;

export const $RawTwinoidUserId: IntegerType = $Uint32;
