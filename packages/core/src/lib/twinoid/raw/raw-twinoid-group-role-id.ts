import { $Sint32, IntegerType } from "kryo/lib/integer.js";

export type RawTwinoidGroupRoleId = number;

export const $RawTwinoidGroupRoleId: IntegerType = $Sint32;
