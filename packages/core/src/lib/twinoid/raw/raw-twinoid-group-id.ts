import { $Uint32, IntegerType } from "kryo/lib/integer.js";

export type RawTwinoidGroupId = number;

export const $RawTwinoidGroupId: IntegerType = $Uint32;
