import { $Uint32, IntegerType } from "kryo/lib/integer.js";

export type RawTwinoidApplicationId = number;

export const $RawTwinoidApplicationId: IntegerType = $Uint32;
